#!/bin/bash
while [ "$select" != "NO" -a "$select" != "YES" ]; do
    select=$(echo -e 'NO\nYES' | dmenu -nb '#151515' -nf '#999999' -sb '#e67e22' -sf '#000000' -fn '-*-*-medium-r-normal-*-*-*-*-*-*-100-*-*' -i -p "Really exit i3?")
        [ -z "$select" ] && exit 0
done

[ "$select" = "NO" ] && exit 0
i3-msg exit
